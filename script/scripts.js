function createNewUser() {
    const user = {};
    user.firstName = prompt('Введіть ім\'я користувача:');
    user.lastName = prompt('Введіть прізвище користувача:');
    user.birthday = prompt('Введіть дату народження (у форматі dd.mm.yyyy):');
    user.getAge = function () {
      const today = new Date();
      const birthDate = new Date(this.birthday.split('.').reverse().join('.'));
      const age = today.getFullYear() - birthDate.getFullYear();
      const monthDiff = today.getMonth() - birthDate.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
        return age - 1;
      }
      return age;
    };
    user.getPassword = function () {
      const firstNameInitial = this.firstName.charAt(0).toUpperCase();
      const lastNameLowercase = this.lastName.toLowerCase();
      const birthYear = this.birthday.split('.')[2];
      return firstNameInitial + lastNameLowercase + birthYear;
    };
    return user;
  }
  const newUser = createNewUser();
  console.log(newUser);
  const age = newUser.getAge();
  console.log(`Вік користувача: ${age} років`);
  const password = newUser.getPassword();
  console.log(`Згенерований пароль: ${password}`);
  